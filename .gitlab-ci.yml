default:
  before_script:
    - echo ${CI_PROJECT_DIR}
    - export PIP_CACHE_DIR="/opt/cache/pip"
    - /opt/conda/bin/conda init && source /root/.bashrc
    - conda config --env --add channels conda-forge
    - conda config --env --append channels defaults
    - >
      if [[ -z $CI_MERGE_REQUEST_TARGET_BRANCH_NAME ]];then
        export COMPARE_BRANCH_NAME="main"
      else
        export COMPARE_BRANCH_NAME=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      fi

stages:
  - style
  - tests
  - build
  - publish

variables:
  SQLALCHEMY_DATABASE_URI: mysql+mysqlconnector://test:test@mariadb:3306/test

check_style:
  stage: style
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/continuumio/miniconda3:latest
  script:
    - pip install black==22.3
    - LC_ALL=C.UTF-8 black --check --safe .

check_lint:
  stage: style
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/continuumio/miniconda3:latest
  script:
    - pip install flake8
    - flake8 src/
  allow_failure: true

test:
  stage: tests
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/continuumio/miniconda3:latest
  services:
    - name: esrfbcu/mimosa-database:main
      alias: mariadb
  script:
    - conda create -q --yes --name test python=3.10
    - conda activate test
    - conda install --file requirements.txt
    - pip install .
    - pytest $PYTEST_ARGS

regenerate:
  stage: build
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/continuumio/miniconda3:latest
  variables:
    MYSQL_USER: test
    MYSQL_PASSWORD: test
    MYSQL_DATABASE: mimosa_build
    MYSQL_ROOT_PASSWORD: rootpassword  
  services:
    - mariadb:10.8
  when:
    manual
  script:
    - |
      conda create -n env python=3.10
      conda activate env
    - |
      apt-get update && apt-get install -y patch curl jq mariadb-client
      CURRENT_VERSION=$(grep __version__ src/mimosa/models/__init__.py | cut -d'"' -f2)
      LATEST_VERSION=$(curl -Ss "https://gitlab.esrf.fr/api/v4/projects/3758/repository/tags" | jq -r '.[0] | .name')
      echo "current: ${CURRENT_VERSION}"
      echo "latest: ${LATEST_VERSION}"
    - |
      NEEDS_UPDATE=0
      if [ "$CURRENT_VERSION" != "${LATEST_VERSION}" ]; then
        NEEDS_UPDATE=1
        SCHEMA_VERSION=$LATEST_VERSION
      fi
    - |
      cat >~/.my.cnf <<EOF
      [client]
      user=root
      host=mariadb
      password=rootpassword
      database=mimosa_build
      EOF
      wget https://gitlab.esrf.fr/ui/mimosa/mimosa-database/-/archive/${SCHEMA_VERSION}/mimosa-database-${SCHEMA_VERSION}.tar.gz
      tar xfz mimosa-database-${SCHEMA_VERSION}.tar.gz
      ls
      cd mimosa-database-${SCHEMA_VERSION}
      ln -s ~/.my.cnf .my.cnf
      ./build.sh
      echo
      echo "Installed tables:"
      mysql -D mimosa_build -e "SHOW TABLES"
      cd ..
      rm -rf mimosa-database-${SCHEMA_VERSION}
      rm mimosa-database-${SCHEMA_VERSION}.tar.gz
      pip install sqlalchemy==1.4.49 sqlacodegen black==22.3 mysql-connector-python==8.0.29 bump2version
      . generate_models.sh
    - | 
      git config user.email "group_4944_bot_7a09b502b2220d38a145e0fc5d72cb45@noreply.gitlab.esrf.fr"
      git config user.name "group_4944_bot_7a09b502b2220d38a145e0fc5d72cb45"
      git remote remove group_origin || true
      git remote add group_origin "https://oauth2:$GROUP_TOKEN@gitlab.esrf.fr/ui/mimosa/mimosa-models.git"
      git add .
      git branch -D "update-schema-$SCHEMA_VERSION" || true
      git checkout -b "update-schema-$SCHEMA_VERSION"
      git commit -m "Update models to schema version $SCHEMA_VERSION"
      bump2version --new-version ${SCHEMA_VERSION} patch
      git push group_origin "update-schema-$SCHEMA_VERSION" -o merge_request.create -o merge_request.target=$CI_COMMIT_REF_NAME -o merge_request.title="Update models to schema version $SCHEMA_VERSION"

sdist:
  stage: build
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/continuumio/miniconda3:latest
  script:
    - pip install setuptools
    - python setup.py sdist
  artifacts:
    paths:
      - dist
    expire_in: 7 days
  only:
    - tags
    - main

pypi:
  stage: publish
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/continuumio/miniconda3:latest
  script:
    - pip install twine
    - twine upload dist/*
  only:
    - tags
