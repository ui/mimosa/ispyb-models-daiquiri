#! /bin/bash

sqlacodegen mysql+mysqlconnector://test:test@mariadb/mimosa_build --nojoined --noinflect --outfile src/mimosa/models/_auto_db_schema.py
black src/mimosa/models/_auto_db_schema.py
patch -p1 src/mimosa/models/_auto_db_schema.py < patches/models.patch
rm src/mimosa/models/_auto_db_schema.py.orig
